# Test Log

Add new tests to the end of the **Test Backlog**
 table as you think of them.

For each TDD cycle (Red-Green-Refactor):

1. Copy the test you are choosing to use to the end of the **Completed Tests** table. Add the next **Test #** and explain why you choose that test for this TDD cycle.
2. Once your all of your code passes all of the tests (to this point), commit the code, tests, and test log with a commit message of "Test #". (Replace # with the actual number from the table.)
3. If you decide that refactoring is needed, do your refactoring, make sure all tests still pass, and make a commit with a commit message describing why you refactored your code.
4. If this is the end of the **Base**, **Intermediate**, or **Advanced** portion of the assignment, add a line to the **Completed Tests** table with **Base**, **Intermediate**, or **Advanced** in the **Test #** column, and make a commit with a message indicating **Base**, **Intermediate**, or **Advanced**.

Repeat until the kata is complete.

## Completed Tests

Test # | Input | Output | Why did you choose this test now?
--- | --- | --- | ---
Test 1 | | |

## Test Backlog

Input | Output
--- | ---
