# Homework 4: Test-Driven Development

Re-read, Kent Beck's post [Canon TDD](https://tidyfirst.substack.com/p/canon-tdd).

Especially pay attention to:

- The 5 steps
- The flowchart *including the mistakes comments*.
- The *Overview*
- The Interface/Implementation Split
- The detailed descriptions of the 5 steps.

You may read/view other blog posts/pages about TDD if you want more information on the process. You should **not** read/view any blog posts/pages specifically about the Word Count Kata.

## Process

1. Follow the instructions in the [Test Log](/testlog.md), completing the tables and making commits as instructed.
2. Create test file(s) in [`src/test/java`](/src/test/java).
3. Create implementation code file(s) in [`src/main/java`](/src/main/java).

### Notes

You should use [Java 11 Standard Library classes](https://docs.oracle.com/en/java/javase/11/docs/api/index.html) whenever it makes sense (eg. data structures, classes, etc.) to make your test pass. There is no reason to implement from scratch anything that is already in the standard library.

You should **not** be using code from other sources, nor should you be following any tutorials or screencasts of someone following the Word Count or similar Katas. The point is for you to experience the TDD process for yourself and see where it leads you and how it might change your process and/or way of thinking.

## *Base Assignment*

**Everyone must complete this portion of the assignment in a *Meets Specification* fashion.**

### Word Count Kata - Base

The input will be a string of text. Non-words (numbers, punctuation) can be ignored. Words are separated by spaces, tabs, new-lines. Word case is ignored.

The output will be a list of all the unique words in the input string, one word per line. The line will the word, followed by how many times that word appeared in the input string.

Example:

Input: "Test and test again until it works."

Output:

test 2<br>
and 1 <br>
again 1<br>
until 1<br>
it 1<br>
works 1<br>

## *Intermediate Add-On*

**Complete this portion in an Acceptable fashion to apply towards base course grades of B or higher.**

* See the the Course Grade Determination table in the syllabus to see how many Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

### Word Count Kata - Intermediate

The **first** input will be a string of text. Non-words (numbers, punctuation) can be ignored. Words are separated by spaces, tabs, new-lines. Word case is ignored.

**The second input will be a string of *stop* words, separated by spaces, tabs, new-lines.**

The output will be a list of all the unique words **(ignoring stop words)** in the input string, one word per line. The line will the word, followed by how many times that word appeared in the input string.

Example:

First Input: "Test and test again until it works."

Second Input: "a the and"

Output:

test 2<br>
again 1<br>
until 1<br>
it 1<br>
works 1<br>

## *Advanced Add-On*

**Complete this portion in an Acceptable fashion to apply towards base course grades of B or higher.**

* See the the Course Grade Determination table in the syllabus to see how many Advanced Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

### Word Count Kata - Advanced

The first input will be a string of text. Non-words (numbers, punctuation) can be ignored. Words are separated by spaces, tabs, new-lines. Word case is ignored.

The second input will be a string of *stop* words, separated by spaces, tabs, new-lines.

The output will be a list of all the unique words (ignoring stop words) in the input string, one word per line. The line will the word, followed by how many times that word appeared in the input string. **The list of words will be sorted by how many times they appear, from most to least.**

Example:

First Input: "Test and test again, and again, and again until it works."

Second Input: "a the and"

Output:

again 3<br>
test 2<br>
until 1<br>
it 1<br>
works 1<br>

## Specification

You must push all of your commits in the correct format with appropriate commit messages to your fork of this project.

## Due Date

17 April 2024 - 23:59

&copy; 2024 Karl R. Wurst <karl@w-sts.com>

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
